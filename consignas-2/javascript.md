#### JAVASCRIPT 

**Resolución teórica (parte 1)**

What will the code below output to the console and why?.

```javascript
var myObject = {
    foo : "bar",
    func:function() {    // función externa
        var self=this;
        console.log("outer func: this.foo = "+this.foo);
        console.log("outer func: self.foo = "+self.foo);
        (function(){ /* función interna (IIFE or Self-Executing) 
                                        -->  ejecución inmediata */
            console.log("inner func: this.foo = " +this.foo);
            console.log("inner func: self.foo = " +self.foo);   
        }());
    }
};

myObject.func();
```
<details>
<summary markdown="span">Respuesta</summary>

<hr>

```bash
$ node test.js
outer func: this.foo = bar
outer func: self.foo = bar
inner func: this.foo = undefined
inner func: self.foo = bar
```
En la función externa, `self` y `this` referencian a `"bar"`. Por lo tanto, se imprimirá
`"bar"` en la terminal.
En la función interna, en cambio, `this` es autorreferencial. `this.foo` no está definido, pero
aún se puede acceder a `self.foo` desde `var self=this` (declarado antes de inner IIFE).

<br>

</details>

**Resolución teórica (parte 2)**

**a.** What is a Promise?.

<details>
<summary markdown="span">Respuesta</summary>

<hr>

Una `Promesa (Promise)` es un objeto que guarda una operación asíncrona:
 - Una petición `AJAX` 
 - El click de un botón, 
 - Una función programada
 - etc.

El objeto generado al crear una `Promesa` es independiente del estado de la llamada asíncrona
(`pending, fulfilled, rejected`), es decir, podemos seguir trabajando con él más allá 
de la duración desconocida de la operación.

<br>

</details>

**b.** What is ECMAScript?.

<details>
<summary markdown="span">Respuesta</summary>

<hr>

Hoy en día son muchas la plataformas que interpretan `JavaScript`. Navegadores web, `NodeJS`
... por nombrar algunas.

Es el estándar `ECMAScript (ES)` el que marca cómo tiene que ser interpretado el lenguaje 
en cada una de estas tecnologías. 

<br>

</details>  

**c.** What is NaN? What is its type? How can you reliably test if a value is equal to NaN?.

<details>
<summary markdown="span">Respuesta</summary>

<hr>

`NaN (Not a Number)` es un valor especial de tipo númerico. Se utiliza para
indicar un error en una función u operación que debería devolver un número válido.

Como `NaN` arroja `False` al compararse con cualquier otro valor e incluso consigo mismo:

`Entrada`
```javascript
console.log(NaN == NaN)
```
`Salida`
```bash
$ node test.js
false
```
La forma más fácil de hacer una verificación es implementando `Number.isNaN()` o `isNaN()`.

<br>

</details>  

**d.**  What will be the output when the following code is executed? Explain.

```javascript 
console.log(false=='0')   // ==  AEC (strict equality)
console.log(false==='0') //  === SEC (loose equality)
```

<details>
<summary markdown="span">Respuesta</summary>

<hr>

`Doble igual` 

   La salida de `false=='0'` será True. Primero convierte los valores en un tipo de datos común (`cast`) y luego compara su igualdad. 

`Triple igual` 

   La salida de `false==='0'` será False. Sin conversión mediante, un valor sólo es igual a sí mismo. 

<br>

</details>