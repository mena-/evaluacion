#### NODE

**Resolución teórica**

Explain what does `"event-driven, non-blocking I/O"` means in Javascript.

<details>
<summary markdown="span">Respuesta</summary>

`event-driven` 

<hr>

Acción en respuesta a `eventos`. El flujo del programa va a estar determinado por 
`eventos`. Cuando se produce un evento, se activa una función callback.

<br>

`non-blocking I/O`

<hr>

Petición de operación `I/O`. El flujo del programa `no se bloquea` y puede continuar
ejecutando tareas mientras se completa la operación asíncrona.

<br>

</details>