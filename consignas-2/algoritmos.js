
const palindrome = str => { 
    s = str.toLowerCase().match(/\w/g);
    return s.join() === [...s].reverse().join();
};

console.log(palindrome('Sé verlas al revés'))