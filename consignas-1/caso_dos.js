
var endorsements = [
    { skill: 'css', user: 'Bill' },
    { skill: 'javascript', user:'Chad' },
    { skill: 'javascript', user: 'Bill' },
    { skill: 'css', user: 'Sue' },
    { skill: 'javascript', user: 'Sue' },
    { skill: 'html', user: 'Sue' },
]

var array_order = [...endorsements.reduce((c, { skill, user }) => {
    if (!c.has(skill)) c.set(skill, { skill, user: [], count: 0 });
    c.get(skill).user.push(user);
    c.get(skill).count++;
    return c;
}, new Map()).values()];

console.log(array_order)