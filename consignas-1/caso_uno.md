#### CASO 1

**Resolución teórica**

¿Cuál es el resultado de las últimas 3 líneas?.

Línea 1
```javascript
// bar()
var Foo = function(a){
    var bar = function() {
        return a;
    }         // correción
};

var f = new Foo (7);
f.bar();
```
<details>
<summary markdown="span">Respuesta</summary>

`TypeError: f.bar is not a function`

<hr>

El resultado es un error. `f.bar` no es una función. Es, en realidad, una expresión de 
función disponible dentro de `Foo`. Básicamente, una función privada con una variable asignada 
(`bar`) viviendo en el ámbito donde fue creada.

<br>

</details>

Línea 2
```javascript
var Foo = function(a){
    // bar() ?
    /* var baz = function() {
        return a;
    }            corregido */ 
    // baz()
    this.baz = function() {
        return a;
    }
};

var f = new Foo(7);
f.baz();
```
<details>
<summary markdown="span">Respuesta</summary>

`Terminal output : 7`

<hr>
 
El resultado es el esperado. 

Exterior

- `new Foo()` invoca la función como `constructor`

Interior

- `this` referencia al `objeto` que creó `new Foo()` 
- `baz` se agrega como una `propiedad` a este `objeto`

En conjunto

- `f.baz` devuelve `var f = new Foo(7)`  

<br>

</details>

Línea 3
```javascript
Foo.prototype = {  /*  compartido con todas las 
                           instancias de Foo...  */
    biz: function() {
        return a;
    }
};

var f = new Foo(7);
f.biz(); /* ...incluso con f. 
            El error no se produce acá!!! */
```
<details>
<summary markdown="span">Respuesta</summary>

`ReferenceError: a is not defined`

<hr>

El resultado es un error. `a` no está definido, al menos no cuando la función
`biz` es convocada. El parámetro `a` únicamente se define en la función constructora, 
como consecuencia de ello -`f.biz`- mostrará una indefinición.

<br>

</details>