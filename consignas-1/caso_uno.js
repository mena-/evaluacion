
var bar
var Foo = function(a) {
    // bar()
    bar = function() {
        return a
    }
    // baz()
    this.baz = function() {
        return a
    }
    // biz()
    Foo.prototype.biz = function() {
        return a
    }
};

var f = new Foo(7)
bar()
f.baz()
f.biz()